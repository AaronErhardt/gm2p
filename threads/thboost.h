/* threadboost.h
 *
 * Copyright (C) 2019-2020 Aaron Erhardt
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __THBOOST
#define __THBOOST

#ifdef __cplusplus
extern "C" {
#endif

/* ==============================  ABOUT THREADBOOST  ============================
 *
 * Threadboost is a highly optimized thread pool that aims to run as fast as possible.
 * In certain situations in can run over 200 times faster than regular thread pools
 * yet its design might have some drawbacks for certain applications.
 */


/* ==================================  THE API  ==================================
 */


/* >>> struct thboost_t
 *
 * This struct stores all relevant information about the thread pool.
 * It is not intended to be accessed manually.
 */

typedef struct pub_thboost_t thboost_t;


/* >>> thboost_t* thboost_init(unsigned int num_of_threads)
 *
 * DESCRIPTION: returns a new thread pool with the specified number of worker threads.
 *
 * RETURN:      a new threadpool.
 *
 * ARGUMENT:    the number of worker threads,
 *              recommended maximum is one less than the number of CPU cores.
 */

thboost_t* thboost_init(unsigned int num_of_threads);


/* >>> void thboost_destroy(thboost_t* th_pool);
 *
 * DESCRIPTION: destroys a given thread pool and frees all resources.
 *
 * ARGUMENT:    pointer to the thread pool that should be destroyed.
 */

void thboost_destroy(thboost_t* th_pool);


/* >>> void thboost_work(thboost_t* th_pool);
 *
 * DESCRIPTION: lets all worker threads do their assigned work,
 *              returns when all worker threads are finished.
 *
 * ARGUMENT:    pointer to the thread pool that should do the work.
 */

void thboost_work(thboost_t* th_pool);


/* >>> void thboost_add_work(thboost_t* th_pool, unsigned int thread_num, void* func, void* args);
 *
 * DESCRIPTION: Assigns work (a function + argument) to a specific worker thread.
 *
 * ARGUMENTS: - th_pool: pointer to the thread pool of the targeted worker thread
 *            - thread_num: number of the worker thread (0 is the first, 1 the second etc.)
 *            - func: function pointer assigned to the worker thread
 *            - args: argument, the assigned function should take (pass multiple arguments as a struct)
 */

void thboost_add_work(thboost_t* th_pool, unsigned int thread_num, void* func, void* args);


/* >>> void thboost_add_work_all(thboost_t* th_pool, void* func, void* args);
 *
 * DESCRIPTION: Assigns the same work (a function + argument) to all worker threads.
 *
 * ARGUMENTS: - th_pool: pointer to the thread pool of the targeted worker threads
 *            - func: function pointer assigned to all worker threads
 *            - args: argument, the assigned function should take (pass multiple arguments as a struct)
 */

void thboost_add_work_all(thboost_t* th_pool, void* func, void* args);


/* >>> void thboost_add_func(thboost_t* th_pool, unsigned int thread_num, void* func);
 *
 * DESCRIPTION: Assigns a function to a specific worker thread.
 *
 * ARGUMENTS: - th_pool: pointer to the thread pool of the targeted worker thread
 *            - thread_num: number of the worker thread (0 is the first, 1 the second etc.)
 *            - func: function pointer assigned to the worker thread
 */

void thboost_add_func(thboost_t* th_pool, unsigned int thread_num, void* func);


/* >>> void thboost_add_func_all(thboost_t* th_pool, void* func);
 *
 * DESCRIPTION: Assigns the same function to all worker threads.
 *
 * ARGUMENTS: - th_pool: pointer to the thread pool of the targeted worker threads
 *            - func: function pointer assigned to all worker threads
 */

void thboost_add_func_all(thboost_t* th_pool, void* func);


/* >>> void thboost_add_args(thboost_t* th_pool, unsigned int thread_num, void* args);
 *
 * DESCRIPTION: Assigns a argument to a specific worker thread.
 *
 * ARGUMENTS: - th_pool: pointer to the thread pool of the targeted worker thread
 *            - thread_num: number of the worker thread (0 is the first, 1 the second etc.)
 *            - args: argument, the assigned function should take (pass multiple arguments as a struct)
 */

void thboost_add_args(thboost_t* th_pool, unsigned int thread_num, void* args);


/* >>> void thboost_add_args_all(thboost_t* th_pool, void* args);
 *
 * DESCRIPTION: Assigns the same argument to all worker threads.
 *
 * ARGUMENTS: - th_pool: pointer to the thread pool of the targeted worker threads
 *            - args: argument, the assigned function should take (pass multiple arguments as a struct)
 */

void thboost_add_args_all(thboost_t* th_pool, void* args);



/* ==============================  THE INTERNAL API  =============================
 *
 * The internal API provides the same functions as the default API (see above) except that the
 * functions have names that start with "thboost_internal..." instead of "thboost..."
 * and always work with the internal thread pool. Therefore you also never need to pass
 * a pointer to a thread pool to functions of the internal API.
 * Since the internal API behaves similar to the default API only a few points documented
 * in this section. For reference have look at the functions of the default API.
 */


/* >>> void thboost_internal_init(unsigned int num_of_threads);
 *
 * NOTE: creates the internal thread pool similar to thboost_init.
 *       This function can be called multiple times but will create the internal
 *       thread pool only if it doesn't exist already.
 *       There can only be ONE INTERNAL THREAD POOL.
 */

void thboost_internal_init(unsigned int num_of_threads);

void thboost_internal_work();

void thboost_internal_add_work(unsigned int thread_num, void* func, void* args);

void thboost_internal_add_work_all(void* func, void* args);

void thboost_internal_add_func(unsigned int thread_num, void* func);

void thboost_internal_add_func_all(void* func);

void thboost_internal_add_args(unsigned int thread_num, void* args);

void thboost_internal_add_args_all(void* args);

void thboost_internal_destroy();

#ifdef __cplusplus
}
#endif

#endif
