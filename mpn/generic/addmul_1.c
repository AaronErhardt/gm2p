/* mpn_addmul_1 -- multiply the N long limb vector pointed to by UP by VL,
   add the N least significant limbs of the product to the limb vector
   pointed to by RP.  Return the most significant limb of the product,
   adjusted for carry-out from the addition.

Copyright 1992-1994, 1996, 2000, 2002, 2004, 2016 Free Software Foundation,
Inc.

This file is part of the GNU MP Library.

The GNU MP Library is free software; you can redistribute it and/or modify
it under the terms of either:

  * the GNU Lesser General Public License as published by the Free
    Software Foundation; either version 3 of the License, or (at your
    option) any later version.

or

  * the GNU General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option) any
    later version.

or both in parallel, as here.

The GNU MP Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received copies of the GNU General Public License and the
GNU Lesser General Public License along with the GNU MP Library.  If not,
see https://www.gnu.org/licenses/.  */

#include "gmp-impl.h"
#include "longlong.h"


#if GMP_NAIL_BITS == 0

mp_limb_t mpn_addmul_1(mp_ptr res_ptr, mp_srcptr a_ptr, mp_size_t a_size,
                       mp_limb_t b_limb_0) {
  mp_limb_t a_limb_0, crec, carry, prod_1, prod_0, res_limb_0;

  ASSERT(a_size >= 1);
  ASSERT(MPN_SAME_OR_SEPARATE_P(res_ptr, a_ptr, a_size));

  crec = 0;
  do {
    a_limb_0 = *a_ptr++;
    umul_ppmm(prod_1, prod_0, a_limb_0, b_limb_0);

    res_limb_0 = *res_ptr;

    prod_0 = res_limb_0 + prod_0;
    carry = res_limb_0 > prod_0;

    prod_1 = prod_1 + carry;

    res_limb_0 = prod_0 + crec;  /* cycle 0, 3, ... */
    carry = prod_0 > res_limb_0; /* cycle 1, 4, ... */

    crec = prod_1 + carry; /* cycle 2, 5, ... */

    *res_ptr++ = res_limb_0;
  } while (--a_size != 0);

  return crec;
}

#endif

#if GMP_NAIL_BITS == 1

mp_limb_t mpn_addmul_1(mp_ptr res_ptr, mp_srcptr a_ptr, mp_size_t a_size,
                       mp_limb_t b_limb_0) {
  mp_limb_t shifted_b_limb_0, a_limb_0, res_limb_0, prod_0, prod_1, prev_prod_1,
      crec, xl, c1, c2, c3;

  ASSERT(a_size >= 1);
  ASSERT(MPN_SAME_OR_SEPARATE_P(res_ptr, a_ptr, a_size));
  ASSERT_MPN(res_ptr, a_size);
  ASSERT_MPN(a_ptr, a_size);
  ASSERT_LIMB(b_limb_0);

  shifted_b_limb_0 = b_limb_0 << GMP_NAIL_BITS;
  crec = 0;
  prev_prod_1 = 0;
  do {
    a_limb_0 = *a_ptr++;
    res_limb_0 = *res_ptr;
    umul_ppmm(prod_1, prod_0, a_limb_0, shifted_b_limb_0);
    prod_0 >>= GMP_NAIL_BITS;
    ADDC_LIMB(c1, xl, prev_prod_1, prod_0);
    ADDC_LIMB(c2, xl, xl, res_limb_0);
    ADDC_LIMB(c3, xl, xl, crec);
    crec = c1 + c2 + c3;
    *res_ptr++ = xl;
    prev_prod_1 = prod_1;
  } while (--a_size != 0);

  return prev_prod_1 + crec;
}

#endif

#if GMP_NAIL_BITS >= 2

mp_limb_t mpn_addmul_1(mp_ptr res_ptr, mp_srcptr a_ptr, mp_size_t a_size,
                       mp_limb_t b_limb_0) {
  mp_limb_t shifted_b_limb_0, a_limb_0, res_limb_0, prod_0, prod_1, prev_prod_1,
      xw, crec, xl;

  ASSERT(a_size >= 1);
  ASSERT(MPN_SAME_OR_SEPARATE_P(res_ptr, a_ptr, a_size));
  ASSERT_MPN(res_ptr, a_size);
  ASSERT_MPN(a_ptr, a_size);
  ASSERT_LIMB(b_limb_0);

  shifted_b_limb_0 = b_limb_0 << GMP_NAIL_BITS;
  crec = 0;
  prev_prod_1 = 0;
  do {
    a_limb_0 = *a_ptr++;
    res_limb_0 = *res_ptr;
    umul_ppmm(prod_1, prod_0, a_limb_0, shifted_b_limb_0);
    prod_0 >>= GMP_NAIL_BITS;
    xw = prev_prod_1 + prod_0 + res_limb_0 + crec;
    crec = xw >> GMP_NUMB_BITS;
    xl = xw & GMP_NUMB_MASK;
    *res_ptr++ = xl;
    prev_prod_1 = prod_1;
  } while (--a_size != 0);

  return prev_prod_1 + crec;
}

#endif
