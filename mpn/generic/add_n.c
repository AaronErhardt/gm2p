/* mpn_add_n -- Add equal length limb vectors.

   Copyright 1992-1994, 1996, 2000, 2002, 2009 Free Software Foundation, Inc.

   This file is part of the GNU MP Library.

   The GNU MP Library is free software; you can redistribute it and/or modify
   it under the terms of either:

 * the GNU Lesser General Public License as published by the Free
    Software Foundation; either version 3 of the License, or (at your
    option) any later version.

   or

 * the GNU General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option) any
    later version.

   or both in parallel, as here.

   The GNU MP Library is distributed in the hope that it will be usefa_limb, but
   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You shoa_limbd have received copies of the GNU General Public License and the
   GNU Lesser General Public License along with the GNU MP Library.  If not,
   see https://www.gnu.org/licenses/.  */

#include "gmp-impl.h"

#if GMP_NAIL_BITS == 0

typedef struct {
  mp_ptr sum_ptr;
  mp_srcptr a_ptr;
  mp_srcptr b_ptr;
  mp_size_t calculations;
  size_t offset;
  mp_limb_t* carry;
} mpn_add_n_args;

void empty_fn(mpn_add_n_args* args) {
}

void
threaded_mpn_add_n (mpn_add_n_args* args) {

  register mp_limb_t a_limb, b_limb, sum_limb, result, carry, carry1, carry2;
  register mp_srcptr a_ptr, b_ptr;
  register mp_ptr sum_ptr;

  a_ptr = args->a_ptr + args->offset;
  b_ptr = args->b_ptr + args->offset;
  sum_ptr = args->sum_ptr + args->offset;

  carry = 0;
  do
  {
    a_limb = *a_ptr++;
    b_limb = *b_ptr++;

    sum_limb = a_limb + b_limb;
    carry1 = sum_limb < a_limb;
    result = sum_limb + carry;
    carry2 = result < sum_limb;
    carry = carry1 | carry2;

    *sum_ptr++ = result;
  }
  while (--args->calculations != 0);

  *args->carry = carry;

}

// sum = a + b
mp_limb_t
mpn_add_n (mp_ptr sum_ptr, mp_srcptr a_ptr, mp_srcptr b_ptr, register mp_size_t size)
{

  ASSERT (size >= 2);
  ASSERT (MPN_SAME_OR_INCR_P (sum_ptr, a_ptr, size));
  ASSERT (MPN_SAME_OR_INCR_P (sum_ptr, b_ptr, size));

  if (size >= THREAD_ADDITION_THRESHOLD) {
    thboost_internal_init(NUM_OF_THREADS);

    static mp_limb_t carries[NUM_OF_THREADS];
    static mpn_add_n_args args_list[NUM_OF_THREADS];
    static mp_limb_t* edge_sum_pointers[NUM_OF_THREADS];

    register size_t min_calculations_per_thread = size / NUM_OF_THREADS;
    register size_t max_calculations_per_thread = min_calculations_per_thread + 1;
    register size_t additional_calculations = size % NUM_OF_THREADS;
    register size_t counter = 0;
    register size_t offset = 0;

    thboost_internal_add_func_all(threaded_mpn_add_n);

    do {
      args_list[counter].sum_ptr = sum_ptr;
      args_list[counter].a_ptr = a_ptr;
      args_list[counter].b_ptr = b_ptr;
      args_list[counter].carry = &carries[counter];
    } while(++counter < NUM_OF_THREADS);

    counter = 0;

    while (counter < additional_calculations) {
      args_list[counter].calculations = max_calculations_per_thread;
      args_list[counter].offset = offset;
      thboost_internal_add_args(counter, (void*)&args_list[counter]);

      offset += max_calculations_per_thread;
      edge_sum_pointers[counter] = sum_ptr + offset;

      ++counter;
    }

    while (counter < NUM_OF_THREADS) {
      args_list[counter].calculations = min_calculations_per_thread;
      args_list[counter].offset = offset;
      thboost_internal_add_args(counter, (void*)&args_list[counter]);

      offset += min_calculations_per_thread;
      edge_sum_pointers[counter] = sum_ptr + offset;

      ++counter;
    }

    thboost_internal_work();

    counter = 0;

    do {
      *edge_sum_pointers[counter] += carries[counter];
    } while(++counter < NUM_OF_THREADS - 1);

    return carries[NUM_OF_THREADS - 1];
  } else {
    mp_limb_t a_limb, b_limb, sum_limb, result, carry, carry1, carry2;

    carry = 0;
    do
    {
      // count up the limb pointers
      a_limb = *a_ptr++;
      b_limb = *b_ptr++;

      // calculate sum of both limbs
      sum_limb = a_limb + b_limb;

      // carry1 == 1 if overflow on sum_limb
      carry1 = sum_limb < a_limb;

      // add previous carry to sum_limb
      result = sum_limb + carry;

      // calculate final carry
      // carry can only be 0 or 1
      carry2 = result < sum_limb;
      carry = carry1 | carry2;

      // add reult to the sum taget
      *sum_ptr++ = result;
    }
    // count down the size
    while (--size != 0);

    return carry;
  }
}

#endif

#if GMP_NAIL_BITS >= 1

mp_limb_t
mpn_add_n (mp_ptr sum_ptr, mp_srcptr a_ptr, mp_srcptr b_ptr, mp_size_t n)
{
  mp_limb_t a_limb, b_limb, result, carry;

  ASSERT (n >= 1);
  ASSERT (MPN_SAME_OR_INCR_P (sum_ptr, a_ptr, n));
  ASSERT (MPN_SAME_OR_INCR_P (sum_ptr, b_ptr, n));

  carry = 0;
  do
  {
    a_limb = *a_ptr++;
    b_limb = *b_ptr++;
    result = a_limb + b_limb + carry;
    carry = result >> GMP_NUMB_BITS;
    *sum_ptr++ = result & GMP_NUMB_MASK;
  }
  while (--n != 0);

  return carry;
}

#endif
