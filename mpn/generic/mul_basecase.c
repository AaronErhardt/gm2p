/* mpn_mul_basecase -- Internal routine to multiply two natural numbers
   of length m and n.

   THIS IS AN INTERNAL FUNCTION WITH A MUTABLE INTERFACE.  IT IS ONLY
   SAFE TO REACH THIS FUNCTION THROUGH DOCUMENTED INTERFACES.

Copyright 1991-1994, 1996, 1997, 2000-2002 Free Software Foundation, Inc.

This file is part of the GNU MP Library.

The GNU MP Library is free software; you can redistribute it and/or modify
it under the terms of either:

  * the GNU Lesser General Public License as published by the Free
    Software Foundation; either version 3 of the License, or (at your
    option) any later version.

or

  * the GNU General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option) any
    later version.

or both in parallel, as here.

The GNU MP Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received copies of the GNU General Public License and the
GNU Lesser General Public License along with the GNU MP Library.  If not,
see https://www.gnu.org/licenses/.  */

#include "gmp-impl.h"
#include <stdlib.h>

/* Multiply {a_ptr,usize} by {b_ptr,vsize} and write the result to
   {prodp,usize+vsize}.  Must have usize>=vsize.

   Note that prodp gets usize+vsize limbs stored, even if the actual result
   only needs usize+vsize-1.

   There's no good reason to call here with vsize>=MUL_TOOM22_THRESHOLD.
   Currently this is allowed, but it might not be in the future.

   This is the most critical code for multiplication.  All multiplies rely
   on this, both small and huge.  Small ones arrive here immediately, huge
   ones arrive here as this is the base case for Karatsuba's recursive
   algorithm.  */

typedef struct {
  mp_ptr *prod_ptr;
  mp_srcptr a_ptr;
  mp_srcptr b_ptr;

  mp_size_t a_size;

  mp_size_t calculations;
  size_t offset;
} mpn_addmul_args;

void mpn_addmul_1_threaded(mpn_addmul_args *args);

void mpn_mul_basecase(mp_ptr prod_ptr, mp_srcptr a_ptr, mp_size_t a_size,
                      mp_srcptr b_ptr, mp_size_t b_size) {
  ASSERT(a_size >= b_size);
  ASSERT(b_size >= 1);
  ASSERT(!MPN_OVERLAP_P(prod_ptr, a_size + b_size, a_ptr, a_size));
  ASSERT(!MPN_OVERLAP_P(prod_ptr, a_size + b_size, b_ptr, b_size));


  /* We first multiply by the low order limb (or depending on optional function
     availability, limbs).  This result can be stored, not added, to prod_ptr.
     We also avoid a loop for zeroing this way.  */

#if HAVE_NATIVE_mpn_mul_2
#ERROR HAVE_NATIVE_mpn_addmul_2 enabled !
  if (b_size >= 2) {
    prod_ptr[a_size + 1] = mpn_mul_2(prod_ptr, a_ptr, a_size, b_ptr);
    prod_ptr += 2, b_ptr += 2, b_size -= 2;
  } else {
    prod_ptr[a_size] = mpn_mul_1(prod_ptr, a_ptr, a_size, b_ptr[0]);
    return;
  }
#else
  prod_ptr[a_size] = mpn_mul_1(prod_ptr, a_ptr, a_size, b_ptr[0]);
  prod_ptr += 1, b_ptr += 1, b_size -= 1;
#endif

  /* Now accumulate the product of a_ptr[] and the next higher limb (or
     depending on optional function availability, limbs) from b_ptr[].  */

#define MAX_LEFT MP_SIZE_T_MAX /* Used to simplify loops into if statements */

#if HAVE_NATIVE_mpn_addmul_6
#ERROR HAVE_NATIVE_mpn_addmul_6 enabled !
  while (b_size >= 6) {
    prod_ptr[a_size + 6 - 1] = mpn_addmul_6(prod_ptr, a_ptr, a_size, b_ptr);
    if (MAX_LEFT == 6)
      return;
    prod_ptr += 6, b_ptr += 6, b_size -= 6;
    if (MAX_LEFT < 2 * 6)
      break;
  }
#undef MAX_LEFT
#define MAX_LEFT (6 - 1)
#endif

#if HAVE_NATIVE_mpn_addmul_5
#ERROR HAVE_NATIVE_mpn_addmul_5 enabled !
  while (b_size >= 5) {
    prod_ptr[a_size + 5 - 1] = mpn_addmul_5(prod_ptr, a_ptr, a_size, b_ptr);
    if (MAX_LEFT == 5)
      return;
    prod_ptr += 5, b_ptr += 5, b_size -= 5;
    if (MAX_LEFT < 2 * 5)
      break;
  }
#undef MAX_LEFT
#define MAX_LEFT (5 - 1)
#endif

#if HAVE_NATIVE_mpn_addmul_4
#ERROR HAVE_NATIVE_mpn_addmul_4 enabled !
  while (b_size >= 4) {
    prod_ptr[a_size + 4 - 1] = mpn_addmul_4(prod_ptr, a_ptr, a_size, b_ptr);
    if (MAX_LEFT == 4)
      return;
    prod_ptr += 4, b_ptr += 4, b_size -= 4;
    if (MAX_LEFT < 2 * 4)
      break;
  }
#undef MAX_LEFT
#define MAX_LEFT (4 - 1)
#endif

#if HAVE_NATIVE_mpn_addmul_3
#ERROR HAVE_NATIVE_mpn_addmul_3 enabled !
  while (b_size >= 3) {
    prod_ptr[a_size + 3 - 1] = mpn_addmul_3(prod_ptr, a_ptr, a_size, b_ptr);
    if (MAX_LEFT == 3)
      return;
    prod_ptr += 3, b_ptr += 3, b_size -= 3;
    if (MAX_LEFT < 2 * 3)
      break;
  }
#undef MAX_LEFT
#define MAX_LEFT (3 - 1)
#endif

#if HAVE_NATIVE_mpn_addmul_2
#ERROR HAVE_NATIVE_mpn_addmul_2 enabled !
  while (b_size >= 2) {
    prod_ptr[a_size + 2 - 1] = mpn_addmul_2(prod_ptr, a_ptr, a_size, b_ptr);
    if (MAX_LEFT == 2)
      return;
    prod_ptr += 2, b_ptr += 2, b_size -= 2;
    if (MAX_LEFT < 2 * 2)
      break;
  }
#undef MAX_LEFT
#define MAX_LEFT (2 - 1)
#endif

  if (b_size >= THREAD_MULTIPLICATION_THRESHOLD) {

    thboost_internal_init(NUM_OF_THREADS);

    static mp_ptr products[NUM_OF_THREADS];
    static mp_size_t sizes[NUM_OF_THREADS];
    static mpn_addmul_args args_list[NUM_OF_THREADS];

    register size_t min_calculations_per_thread = b_size / NUM_OF_THREADS;
    register size_t max_calculations_per_thread =
        min_calculations_per_thread + 1;
    register size_t additional_calculations = b_size % NUM_OF_THREADS;
    register size_t counter = 0;
    register size_t offset = 0;

    do {
      args_list[counter].prod_ptr = &products[counter];
      args_list[counter].a_ptr = a_ptr;
      args_list[counter].b_ptr = b_ptr;
      args_list[counter].a_size = a_size;
    } while (++counter < NUM_OF_THREADS);

    counter = 0;

    while (counter < additional_calculations) {
      args_list[counter].calculations = max_calculations_per_thread;
      args_list[counter].offset = offset;
      thboost_internal_add_work(counter, mpn_addmul_1_threaded,
                                (void *)&args_list[counter]);

      offset += max_calculations_per_thread;
      sizes[counter] = max_calculations_per_thread;

      ++counter;
    }

    while (counter < NUM_OF_THREADS) {
      args_list[counter].calculations = min_calculations_per_thread;
      args_list[counter].offset = offset;
      thboost_internal_add_work(counter, mpn_addmul_1_threaded,
                                (void *)&args_list[counter]);

      offset += min_calculations_per_thread;
      sizes[counter] = min_calculations_per_thread;

      ++counter;
    }

    thboost_internal_work();

    counter = 0;
    register mp_limb_t a_limb, b_limb, sum_limb, result, carry, carry1, carry2;

    carry = 0;

    // memset(prod_ptr, 0, sizeof(mp_limb_t) * (a_size + b_size));

    do {

      a_ptr = products[counter];
      mp_size_t size = sizes[counter] + a_size;

      do {
        // count up the limb pointers
        a_limb = *a_ptr++;
        b_limb = *prod_ptr;

        // calculate sum of both limbs
        sum_limb = a_limb + b_limb;

        // carry1 == 1 if overflow on sum_limb
        carry1 = sum_limb < a_limb;

        // add previous carry to sum_limb
        result = sum_limb + carry;

        // calculate final carry
        // carry can only be 0 or 1
        carry2 = result < sum_limb;
        carry = carry1 | carry2;

        // add reult to the sum taget
        *prod_ptr++ = result;
      }
      // count down the size
      while (--size != 0);

      free(products[counter]);

      if (++counter < NUM_OF_THREADS) {
        *prod_ptr = carry;
      } else {
        break;
      }

      prod_ptr -= a_size;

    } while (1);

  } else {

    while (b_size >= 1) {
      prod_ptr[a_size] = mpn_addmul_1(prod_ptr, a_ptr, a_size, b_ptr[0]);
      prod_ptr += 1, b_ptr += 1, b_size -= 1;
    }
  }
}

void mpn_addmul_1_threaded(mpn_addmul_args *args) {

  register mp_ptr prod_ptr = (mp_limb_t *)calloc(
      (args->a_size + args->calculations), sizeof(mp_limb_t));

  *(args->prod_ptr) = prod_ptr;

  register mp_size_t a_size = args->a_size;
  register mp_srcptr b_ptr = args->b_ptr + args->offset;
  register mp_srcptr a_ptr = args->a_ptr; // + args->offset;

  do {
    prod_ptr[a_size] = mpn_addmul_1(prod_ptr, a_ptr, a_size, b_ptr[0]);

    prod_ptr += 1;
    b_ptr += 1;
    args->calculations -= 1;
  } while (args->calculations >= 1);
}
