/* mpn_mul -- Multiply two natural numbers.

   Contributed to the GNU project by Torbjorn Granlund.

Copyright 1991, 1993, 1994, 1996, 1997, 1999-2003, 2005-2007, 2009, 2010, 2012,
2014, 2019 Free Software Foundation, Inc.

This file is part of the GNU MP Library.

The GNU MP Library is free software; you can redistribute it and/or modify
it under the terms of either:

  * the GNU Lesser General Public License as published by the Free
    Software Foundation; either version 3 of the License, or (at your
    option) any later version.

or

  * the GNU General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option) any
    later version.

or both in parallel, as here.

The GNU MP Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received copies of the GNU General Public License and the
GNU Lesser General Public License along with the GNU MP Library.  If not,
see https://www.gnu.org/licenses/.  */

#include "gmp-impl.h"


#ifndef MUL_BASECASE_MAX_UN
#define MUL_BASECASE_MAX_UN 500
#endif

/* Areas where the different toom algorithms can be called (extracted
   from the t-toom*.c files, and ignoring small constant offsets):

   1/6  1/5 1/4 4/13 1/3 3/8 2/5 5/11 1/2 3/5 2/3 3/4 4/5   1 vn/un
                                        4/7              6/7
				       6/11
                                       |--------------------| toom22 (small)
                                                           || toom22 (large)
                                                       |xxxx| toom22 called
                      |-------------------------------------| toom32
                                         |xxxxxxxxxxxxxxxx| | toom32 called
                                               |------------| toom33
                                                          |x| toom33 called
             |---------------------------------|            | toom42
	              |xxxxxxxxxxxxxxxxxxxxxxxx|            | toom42 called
                                       |--------------------| toom43
                                               |xxxxxxxxxx|   toom43 called
         |-----------------------------|                      toom52 (a_sizeused)
                                                   |--------| toom44
						   |xxxxxxxx| toom44 called
                              |--------------------|        | toom53
                                        |xxxxxx|              toom53 called
    |-------------------------|                               toom62 (a_sizeused)
                                           |----------------| toom54 (a_sizeused)
                      |--------------------|                  toom63
	                      |xxxxxxxxx|                   | toom63 called
                          |---------------------------------| toom6h
						   |xxxxxxxx| toom6h called
                                  |-------------------------| toom8h (32 bit)
                 |------------------------------------------| toom8h (64 bit)
						   |xxxxxxxx| toom8h called
*/

#define TOOM33_OK(an,bn) (6 + 2 * an < 3 * bn)
#define TOOM44_OK(an,bn) (12 + 3 * an < 4 * bn)

/* Multiply the natural numbers u (pointed to by A_PTR, with B_SIZE limbs) and v
   (pointed to by B_PTR, with B_SIZE limbs), and store the result at PROD_PTR.  The
   result is B_SIZE + B_SIZE limbs.  Return the most significant limb of the result.

   NOTE: The space pointed to by PROD_PTR is overwritten before finished with U
   and V, so overlap is an error.

   Argument constraints:
   1. B_SIZE >= B_SIZE.
   2. PROD_PTR != A_PTR and PROD_PTR != B_PTR, i.e. the destination must be distinct from
      the multiplier and the multiplicand.  */

/*
  * The cutoff lines in the toomX2 and toomX3 code are now exactly between the
    ideal lines of the surrounding algorithms.  Is that optimal?

  * The toomX3 code now uses a structure similar to the one of toomX2, except
    that it loops longer in the unbalanced case.  The result is that the
    remaining area might have a_size < b_size.  Should we fix the toomX2 code in a
    similar way?

  * The toomX3 code is used for the largest non-FFT unbalanced operands.  It
    therefore calls mpn_mul recursively for certain cases.

  * Allocate static temp space using THRESHOLD variables (except for toom44
    when !WANT_FFT).  That way, we can typically have no TMP_ALLOC at all.

  * We sort ToomX2 algorithms together, assuming the toom22, toom32, toom42
    have the same b_size threshold.  This is not true, we should actually use
    mul_basecase for slightly larger operands for toom32 than for toom22, and
    even larger for toom42.

  * That problem is even more prevalent for toomX3.  We therefore use special
    THRESHOLD variables there.
*/

mp_limb_t
mpn_mul (mp_ptr prod_ptr,
	 mp_srcptr a_ptr, mp_size_t a_size,
	 mp_srcptr b_ptr, mp_size_t b_size)
{
  ASSERT (a_size >= b_size);
  ASSERT (b_size >= 1);
  ASSERT (! MPN_OVERLAP_P (prod_ptr, a_size+b_size, a_ptr, a_size));
  ASSERT (! MPN_OVERLAP_P (prod_ptr, a_size+b_size, b_ptr, b_size));

  if (BELOW_THRESHOLD (a_size, MUL_TOOM22_THRESHOLD))
    {
      /* When a_size (and thus b_size) is below the toom22 range, do mul_basecase.
	 Test a_size and not b_size here not to thwart the a_size >> b_size code below.
	 This special case is not necessary, but cuts the overhead for the
	 smallest operands. */
      mpn_mul_basecase (prod_ptr, a_ptr, a_size, b_ptr, b_size);
    }
  else if (a_size == b_size)
    {
      mpn_mul_n (prod_ptr, a_ptr, b_ptr, a_size);
    }
  else if (b_size < MUL_TOOM22_THRESHOLD)
    { /* plain schoolbook multiplication */

      /* Unless a_size is very large, or else if have an applicable mpn_mul_N,
	 perform basecase multiply directly.  */
      if (a_size <= MUL_BASECASE_MAX_UN
#if HAVE_NATIVE_mpn_mul_2
	  || b_size <= 2
#else
	  || b_size == 1
#endif
	  )
	mpn_mul_basecase (prod_ptr, a_ptr, a_size, b_ptr, b_size);
      else
	{
	  /* We have a_size >> MUL_BASECASE_MAX_UN > b_size.  For better memory
	     locality, split a_ptr[] into MUL_BASECASE_MAX_UN pieces and multiply
	     these pieces with the b_ptr[] operand.  After each such partial
	     multiplication (but the last) we copy the most significant b_size
	     limbs into a temporary buffer since that part would otherwise be
	     overwritten by the next multiplication.  After the next
	     multiplication, we add it back.  This illustrates the situation:

                                                    -->b_size<--
                                                      |  |<----- a_size ----->|
                                                         _____________________|
                                                        X                    /|
                                                      /XX__________________/  |
                                    _____________________                     |
                                   X                    /                     |
                                 /XX__________________/                       |
               _____________________                                          |
              /                    /                                          |
            /____________________/                                            |
	    ==================================================================

	    The parts marked with X are the parts whose sums are copied into
	    the temporary buffer.  */

	  mp_limb_t tp[MUL_TOOM22_THRESHOLD_LIMIT];
	  mp_limb_t cy;
	  ASSERT (MUL_TOOM22_THRESHOLD <= MUL_TOOM22_THRESHOLD_LIMIT);

	  mpn_mul_basecase (prod_ptr, a_ptr, MUL_BASECASE_MAX_UN, b_ptr, b_size);
	  prod_ptr += MUL_BASECASE_MAX_UN;
	  MPN_COPY (tp, prod_ptr, b_size);		/* preserve high triangle */
	  a_ptr += MUL_BASECASE_MAX_UN;
	  a_size -= MUL_BASECASE_MAX_UN;
	  while (a_size > MUL_BASECASE_MAX_UN)
	    {
	      mpn_mul_basecase (prod_ptr, a_ptr, MUL_BASECASE_MAX_UN, b_ptr, b_size);
	      cy = mpn_add_n (prod_ptr, prod_ptr, tp, b_size); /* add back preserved triangle */
	      mpn_incr_u (prod_ptr + b_size, cy);
	      prod_ptr += MUL_BASECASE_MAX_UN;
	      MPN_COPY (tp, prod_ptr, b_size);		/* preserve high triangle */
	      a_ptr += MUL_BASECASE_MAX_UN;
	      a_size -= MUL_BASECASE_MAX_UN;
	    }
	  if (a_size > b_size)
	    {
	      mpn_mul_basecase (prod_ptr, a_ptr, a_size, b_ptr, b_size);
	    }
	  else
	    {
	      ASSERT (a_size > 0);
	      mpn_mul_basecase (prod_ptr, b_ptr, b_size, a_ptr, a_size);
	    }
	  cy = mpn_add_n (prod_ptr, prod_ptr, tp, b_size); /* add back preserved triangle */
	  mpn_incr_u (prod_ptr + b_size, cy);
	}
    }
  else if (BELOW_THRESHOLD (b_size, MUL_TOOM33_THRESHOLD))
    {
      /* Use ToomX2 variants */
      mp_ptr scratch;
      TMP_SDECL; TMP_SMARK;

#define ITCH_TOOMX2 (9 * b_size / 2 + GMP_NUMB_BITS * 2)
      scratch = TMP_SALLOC_LIMBS (ITCH_TOOMX2);
      ASSERT (mpn_toom22_mul_itch ((5*b_size-1)/4, b_size) <= ITCH_TOOMX2); /* 5b_size/2+ */
      ASSERT (mpn_toom32_mul_itch ((7*b_size-1)/4, b_size) <= ITCH_TOOMX2); /* 7b_size/6+ */
      ASSERT (mpn_toom42_mul_itch (3 * b_size - 1, b_size) <= ITCH_TOOMX2); /* 9b_size/2+ */
#undef ITCH_TOOMX2

      /* FIXME: This condition (repeated in the loop below) leaves from a b_size*b_size
	 square to a (3b_size-1)*b_size rectangle.  Leaving such a rectangle is hardly
	 will sometimes end a_ptr with a_size < b_size, like in the X3 arm below.  */
      if (a_size >= 3 * b_size)
	{
	  mp_limb_t cy;
	  mp_ptr ws;

	  /* The maximum ws usage is for the mpn_mul result.  */
	  ws = TMP_SALLOC_LIMBS (4 * b_size);

	  mpn_toom42_mul (prod_ptr, a_ptr, 2 * b_size, b_ptr, b_size, scratch);
	  a_size -= 2 * b_size;
	  a_ptr += 2 * b_size;
	  prod_ptr += 2 * b_size;

	  while (a_size >= 3 * b_size)
	    {
	      mpn_toom42_mul (ws, a_ptr, 2 * b_size, b_ptr, b_size, scratch);
	      a_size -= 2 * b_size;
	      a_ptr += 2 * b_size;
	      cy = mpn_add_n (prod_ptr, prod_ptr, ws, b_size);
	      MPN_COPY (prod_ptr + b_size, ws + b_size, 2 * b_size);
	      mpn_incr_u (prod_ptr + b_size, cy);
	      prod_ptr += 2 * b_size;
	    }

	  /* b_size <= a_size < 3b_size */

	  if (4 * a_size < 5 * b_size)
	    mpn_toom22_mul (ws, a_ptr, a_size, b_ptr, b_size, scratch);
	  else if (4 * a_size < 7 * b_size)
	    mpn_toom32_mul (ws, a_ptr, a_size, b_ptr, b_size, scratch);
	  else
	    mpn_toom42_mul (ws, a_ptr, a_size, b_ptr, b_size, scratch);

	  cy = mpn_add_n (prod_ptr, prod_ptr, ws, b_size);
	  MPN_COPY (prod_ptr + b_size, ws + b_size, a_size);
	  mpn_incr_u (prod_ptr + b_size, cy);
	}
      else
	{
	  if (4 * a_size < 5 * b_size)
	    mpn_toom22_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size, scratch);
	  else if (4 * a_size < 7 * b_size)
	    mpn_toom32_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size, scratch);
	  else
	    mpn_toom42_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size, scratch);
	}
      TMP_SFREE;
    }
  else if (BELOW_THRESHOLD ((a_size + b_size) >> 1, MUL_FFT_THRESHOLD) ||
	   BELOW_THRESHOLD (3 * b_size, MUL_FFT_THRESHOLD))
    {
      /* Handle the largest operands that are not in the FFT range.  The 2nd
	 condition makes very a_sizebalanced operands avoid the FFT code (except
	 perhaps as coefficient products of the Toom code.  */

      if (BELOW_THRESHOLD (b_size, MUL_TOOM44_THRESHOLD) || !TOOM44_OK (a_size, b_size))
	{
	  /* Use ToomX3 variants */
	  mp_ptr scratch;
	  TMP_DECL; TMP_MARK;

#define ITCH_TOOMX3 (4 * b_size + GMP_NUMB_BITS)
	  scratch = TMP_ALLOC_LIMBS (ITCH_TOOMX3);
	  ASSERT (mpn_toom33_mul_itch ((7*b_size-1)/6, b_size) <= ITCH_TOOMX3); /* 7b_size/2+ */
	  ASSERT (mpn_toom43_mul_itch ((3*b_size-1)/2, b_size) <= ITCH_TOOMX3); /* 9b_size/4+ */
	  ASSERT (mpn_toom32_mul_itch ((7*b_size-1)/4, b_size) <= ITCH_TOOMX3); /* 7b_size/6+ */
	  ASSERT (mpn_toom53_mul_itch ((11*b_size-1)/6, b_size) <= ITCH_TOOMX3); /* 11b_size/3+ */
	  ASSERT (mpn_toom42_mul_itch ((5*b_size-1)/2, b_size) <= ITCH_TOOMX3); /* 15b_size/4+ */
	  ASSERT (mpn_toom63_mul_itch ((5*b_size-1)/2, b_size) <= ITCH_TOOMX3); /* 15b_size/4+ */
#undef ITCH_TOOMX3

	  if (2 * a_size >= 5 * b_size)
	    {
	      mp_limb_t cy;
	      mp_ptr ws;

	      /* The maximum ws usage is for the mpn_mul result.  */
	      ws = TMP_ALLOC_LIMBS (7 * b_size >> 1);

	      if (BELOW_THRESHOLD (b_size, MUL_TOOM42_TO_TOOM63_THRESHOLD))
		mpn_toom42_mul (prod_ptr, a_ptr, 2 * b_size, b_ptr, b_size, scratch);
	      else
		mpn_toom63_mul (prod_ptr, a_ptr, 2 * b_size, b_ptr, b_size, scratch);
	      a_size -= 2 * b_size;
	      a_ptr += 2 * b_size;
	      prod_ptr += 2 * b_size;

	      while (2 * a_size >= 5 * b_size)	/* a_size >= 2.5b_size */
		{
		  if (BELOW_THRESHOLD (b_size, MUL_TOOM42_TO_TOOM63_THRESHOLD))
		    mpn_toom42_mul (ws, a_ptr, 2 * b_size, b_ptr, b_size, scratch);
		  else
		    mpn_toom63_mul (ws, a_ptr, 2 * b_size, b_ptr, b_size, scratch);
		  a_size -= 2 * b_size;
		  a_ptr += 2 * b_size;
		  cy = mpn_add_n (prod_ptr, prod_ptr, ws, b_size);
		  MPN_COPY (prod_ptr + b_size, ws + b_size, 2 * b_size);
		  mpn_incr_u (prod_ptr + b_size, cy);
		  prod_ptr += 2 * b_size;
		}

	      /* b_size / 2 <= a_size < 2.5b_size */

	      if (a_size < b_size)
		mpn_mul (ws, b_ptr, b_size, a_ptr, a_size);
	      else
		mpn_mul (ws, a_ptr, a_size, b_ptr, b_size);

	      cy = mpn_add_n (prod_ptr, prod_ptr, ws, b_size);
	      MPN_COPY (prod_ptr + b_size, ws + b_size, a_size);
	      mpn_incr_u (prod_ptr + b_size, cy);
	    }
	  else
	    {
	      if (6 * a_size < 7 * b_size)
		mpn_toom33_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size, scratch);
	      else if (2 * a_size < 3 * b_size)
		{
		  if (BELOW_THRESHOLD (b_size, MUL_TOOM32_TO_TOOM43_THRESHOLD))
		    mpn_toom32_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size, scratch);
		  else
		    mpn_toom43_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size, scratch);
		}
	      else if (6 * a_size < 11 * b_size)
		{
		  if (4 * a_size < 7 * b_size)
		    {
		      if (BELOW_THRESHOLD (b_size, MUL_TOOM32_TO_TOOM53_THRESHOLD))
			mpn_toom32_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size, scratch);
		      else
			mpn_toom53_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size, scratch);
		    }
		  else
		    {
		      if (BELOW_THRESHOLD (b_size, MUL_TOOM42_TO_TOOM53_THRESHOLD))
			mpn_toom42_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size, scratch);
		      else
			mpn_toom53_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size, scratch);
		    }
		}
	      else
		{
		  if (BELOW_THRESHOLD (b_size, MUL_TOOM42_TO_TOOM63_THRESHOLD))
		    mpn_toom42_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size, scratch);
		  else
		    mpn_toom63_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size, scratch);
		}
	    }
	  TMP_FREE;
	}
      else
	{
	  mp_ptr scratch;
	  TMP_DECL; TMP_MARK;

	  if (BELOW_THRESHOLD (b_size, MUL_TOOM6H_THRESHOLD))
	    {
	      scratch = TMP_SALLOC_LIMBS (mpn_toom44_mul_itch (a_size, b_size));
	      mpn_toom44_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size, scratch);
	    }
	  else if (BELOW_THRESHOLD (b_size, MUL_TOOM8H_THRESHOLD))
	    {
	      scratch = TMP_SALLOC_LIMBS (mpn_toom6h_mul_itch (a_size, b_size));
	      mpn_toom6h_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size, scratch);
	    }
	  else
	    {
	      scratch = TMP_ALLOC_LIMBS (mpn_toom8h_mul_itch (a_size, b_size));
	      mpn_toom8h_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size, scratch);
	    }
	  TMP_FREE;
	}
    }
  else
    {
      if (a_size >= 8 * b_size)
	{
	  mp_limb_t cy;
	  mp_ptr ws;
	  TMP_DECL; TMP_MARK;

	  /* The maximum ws usage is for the mpn_mul result.  */
	  ws = TMP_BALLOC_LIMBS (9 * b_size >> 1);

	  mpn_fft_mul (prod_ptr, a_ptr, 3 * b_size, b_ptr, b_size);
	  a_size -= 3 * b_size;
	  a_ptr += 3 * b_size;
	  prod_ptr += 3 * b_size;

	  while (2 * a_size >= 7 * b_size)	/* a_size >= 3.5b_size  */
	    {
	      mpn_fft_mul (ws, a_ptr, 3 * b_size, b_ptr, b_size);
	      a_size -= 3 * b_size;
	      a_ptr += 3 * b_size;
	      cy = mpn_add_n (prod_ptr, prod_ptr, ws, b_size);
	      MPN_COPY (prod_ptr + b_size, ws + b_size, 3 * b_size);
	      mpn_incr_u (prod_ptr + b_size, cy);
	      prod_ptr += 3 * b_size;
	    }

	  /* b_size / 2 <= a_size < 3.5b_size */

	  if (a_size < b_size)
	    mpn_mul (ws, b_ptr, b_size, a_ptr, a_size);
	  else
	    mpn_mul (ws, a_ptr, a_size, b_ptr, b_size);

	  cy = mpn_add_n (prod_ptr, prod_ptr, ws, b_size);
	  MPN_COPY (prod_ptr + b_size, ws + b_size, a_size);
	  mpn_incr_u (prod_ptr + b_size, cy);

	  TMP_FREE;
	}
      else
	mpn_fft_mul (prod_ptr, a_ptr, a_size, b_ptr, b_size);
    }

  return prod_ptr[a_size + b_size - 1];	/* historic */
}
