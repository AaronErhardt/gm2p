/* mpn_mul_1 -- Multiply a limb vector with a single limb and store the
   product in a second limb vector.

Copyright 1991-1994, 1996, 2000-2002 Free Software Foundation, Inc.

This file is part of the GNU MP Library.

The GNU MP Library is free software; you can redistribute it and/or modify
it under the terms of either:

  * the GNU Lesser General Public License as published by the Free
    Software Foundation; either version 3 of the License, or (at your
    option) any later version.

or

  * the GNU General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option) any
    later version.

or both in parallel, as here.

The GNU MP Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received copies of the GNU General Public License and the
GNU Lesser General Public License along with the GNU MP Library.  If not,
see https://www.gnu.org/licenses/.  */

#include "gmp-impl.h"
#include "longlong.h"

#if GMP_NAIL_BITS == 0

mp_limb_t
mpn_mul_1 (mp_ptr res_ptr, mp_srcptr a_limbs, mp_size_t a_size, mp_limb_t b_limb)
{

  mp_limb_t a_limb, carry_limb, high_prod_limb, low_prod_limb;

  ASSERT (a_size >= 1);
  ASSERT (MPN_SAME_OR_INCR_P (res_ptr, a_limbs, a_size));

  carry_limb = 0;
  do
    {
      a_limb = *a_limbs++;
      umul_ppmm (high_prod_limb, low_prod_limb, a_limb, b_limb);

      low_prod_limb += carry_limb;
      carry_limb = (low_prod_limb < carry_limb) + high_prod_limb;

      *res_ptr++ = low_prod_limb;
    }
  while (--a_size != 0);

  return carry_limb;
}

#endif

#if GMP_NAIL_BITS >= 1

mp_limb_t
mpn_mul_1 (mp_ptr res_ptr, mp_srcptr a_limbs, mp_size_t a_size, mp_limb_t b_limb)
{
  mp_limb_t shifted_b_limb, a_limb, low_prod_limb, high_prod_limb, prev_high_prod_limb, xw, carry_limb, xl;

  ASSERT (a_size >= 1);
  ASSERT (MPN_SAME_OR_INCR_P (res_ptr, a_limbs, a_size));
  ASSERT_MPN (a_limbs, a_size);
  ASSERT_LIMB (b_limb);

  shifted_b_limb = b_limb << GMP_NAIL_BITS;
  carry_limb = 0;
  prev_high_prod_limb = 0;
  do
    {
      a_limb = *a_limbs++;

      umul_ppmm (high_prod_limb, low_prod_limb, a_limb, shifted_b_limb);
      low_prod_limb >>= GMP_NAIL_BITS;
      xw = prev_high_prod_limb + low_prod_limb + carry_limb;
      carry_limb = xw >> GMP_NUMB_BITS;
      xl = xw & GMP_NUMB_MASK;
      *res_ptr++ = xl;
      prev_high_prod_limb = high_prod_limb;
    }
  while (--a_size != 0);

  return prev_high_prod_limb + carry_limb;
}

#endif
