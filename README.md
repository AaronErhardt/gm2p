# GM2P

The GM2P (GNU Multithreaded Multiple Precision Arithmetic Library) is a fork of the GMP (GNU Multiple Precision Arithmetic Library) that aims to improve its performance by using multiple threads to perform calculations in parallel. Because the GM2P is built directly into the GMP library it can be used as drop-in replacement for the GMP in existing projects.

## Expectable performance improvements

The GMP is heavily optimized and uses a lot of handwritten assembler optimizations. Therefore it performs incredibly efficient for most calculations and should be faster than alternative libraries.

The GM2P however can even outperform the GMP by using multiple CPU cores simultaneously. On my machine with a Xeon E3-1240 CPU and 3 worker threads, additions performed faster for numbers with over 1400 decimal digits. Multiplications on the other side did already perform faster for numbers with over 70 decimal digits.

Benchmarks on other systems might vary from my results. Please test the GM2P on your own system and with your existing code base before considering to use it in production! If you're only using small numbers (up to a maximum of around 70 decimal digits or 256 binary digits) we recommend to use the GMP or other multi precision libraries instead. 

The GM2P uses a highly optimized thread pool called Threadboost to archive high performance. More information is available at the projects GitLab Repo: [https://gitlab.com/AaronErhardt/threadboost](https://gitlab.com/AaronErhardt/threadboost)

### Functions that use multiple threads
+ mpn_add_n (this also affects most higher-level addition operations)
+ mpn_mul_basecase (this also affects most higher-level multiplication operations)

# Build and install

Configure:

`./configure --enable-maintainer-mode --enable-assembly=no`

Compile:

`make`

Install:

`make install`

## Adjust the number of worker threads and thresholds

A macro in line 48 of the file gmp-impl.h stores the number of worker threads the GM2P will use. The number should not be larger than the number of CPU cores - 1.

In the following lines you can also adjust the threshold that enables multithreaded operations for addition and multiplication.
