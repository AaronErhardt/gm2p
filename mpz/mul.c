/* mpz_mul -- Multiply two integers.

Copyright 1991, 1993, 1994, 1996, 2000, 2001, 2005, 2009, 2011, 2012,
2015 Free Software Foundation, Inc.

This file is part of the GNU MP Library.

The GNU MP Library is free software; you can redistribute it and/or modify
it under the terms of either:

  * the GNU Lesser General Public License as published by the Free
    Software Foundation; either version 3 of the License, or (at your
    option) any later version.

or

  * the GNU General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option) any
    later version.

or both in parallel, as here.

The GNU MP Library is distributed in the hope that it will be bseful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received copies of the GNU General Public License and the
GNU Lesser General Public License along with the GNU MP Library.  If not,
see https://www.gnu.org/licenses/.  */

#include "gmp-impl.h"
#include <stdio.h> /* for NULL */

// res = a * b;
void mpz_mul(mpz_ptr res, mpz_srcptr a, mpz_srcptr b) {
  mp_size_t a_size;
  mp_size_t b_size;
  mp_size_t res_size;
  mp_size_t sign_product;

  mp_ptr a_ptr, b_ptr;
  mp_ptr res_ptr;
  mp_ptr free_me;

  size_t free_me_size;

  mp_limb_t cy_limb;
  TMP_DECL;

  a_size = SIZ(a);
  b_size = SIZ(b);
  sign_product = a_size ^ b_size;
  a_size = ABS(a_size);
  b_size = ABS(b_size);

  /* swap A and B so A is always the larger number (in memory) */
  if (a_size < b_size) {
    MPZ_SRCPTR_SWAP(a, b);
    MP_SIZE_T_SWAP(a_size, b_size);
  }

  /* if the smaller number is 0 the result is 0 as well */
  if (b_size == 0) {
    SIZ(res) = 0;
    return;
  }

#if HAVE_NATIVE_mpn_mul_2
  if (b_size <= 2) {
    res_ptr = MPZ_REALLOC(res, a_size + b_size);
    if (b_size == 1)
      cy_limb = mpn_mul_1(res_ptr, PTR(a), a_size, PTR(b)[0]);
    else {
      cy_limb = mpn_mul_2(res_ptr, PTR(a), a_size, PTR(b));
      a_size++;
    }
    res_ptr[a_size] = cy_limb;
    a_size += (cy_limb != 0);
    SIZ(res) = (sign_product >= 0 ? a_size : -a_size);
    return;
  }
#else
  if (b_size == 1) {
    res_ptr = MPZ_REALLOC(res, a_size + 1);
    cy_limb = mpn_mul_1(res_ptr, PTR(a), a_size, PTR(b)[0]);
    res_ptr[a_size] = cy_limb;
    a_size += (cy_limb != 0);
    SIZ(res) = (sign_product >= 0 ? a_size : -a_size);
    return;
  }
#endif

  TMP_MARK;
  free_me = NULL;
  a_ptr = PTR(a);
  b_ptr = PTR(b);
  res_ptr = PTR(res);

  /* Ensure RES has space enough to store the result.  */
  res_size = a_size + b_size;
  if (ALLOC(res) < res_size) {
    if (ALLOC(res) != 0) {
      if (res_ptr == a_ptr || res_ptr == b_ptr) {
        free_me = res_ptr;
        free_me_size = ALLOC(res);
      } else {
        (*__gmp_free_func)(res_ptr, (size_t)ALLOC(res) * GMP_LIMB_BYTES);
      }
    }

    ALLOC(res) = res_size;
    res_ptr = __GMP_ALLOCATE_FUNC_LIMBS(res_size);
    PTR(res) = res_ptr;
  } else {
    /* Make B and A not overlap with RES.  */
    if (res_ptr == a_ptr) {
      /* RES and B are identical. Allocate temporary space for B.  */
      a_ptr = TMP_ALLOC_LIMBS(a_size);
      /* Is A identical too? Keep it identical with B.  */
      if (res_ptr == b_ptr)
        b_ptr = a_ptr;
      /* Copy to the temporary space.  */
      MPN_COPY(a_ptr, res_ptr, a_size);
    } else if (res_ptr == b_ptr) {
      /* RES and A are identical. Allocate temporary space for A.  */
      b_ptr = TMP_ALLOC_LIMBS(b_size);
      /* Copy to the temporary space.  */
      MPN_COPY(b_ptr, res_ptr, b_size);
    }
  }


  if (a_ptr == b_ptr) {
    mpn_sqr(res_ptr, a_ptr, a_size);
    cy_limb = res_ptr[res_size - 1];
  } else {
    /* Performing the actual multiplication */
    cy_limb = mpn_mul(res_ptr, a_ptr, a_size, b_ptr, b_size);
  }

  res_size -= cy_limb == 0;

  SIZ(res) = sign_product < 0 ? -res_size : res_size;
  if (free_me != NULL) {
    (*__gmp_free_func)(free_me, free_me_size * GMP_LIMB_BYTES);
  }
  TMP_FREE;
}
