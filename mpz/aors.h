/* mpz_add, mpz_sub -- add or subtract integers.

   Copyright 1991, 1993, 1994, 1996, 2000, 2001, 2011, 2012 Free Software
   Foundation, Inc.

   This file is part of the GNU MP Library.

   The GNU MP Library is free software; you can redistribute it and/or modify
   it under the terms of either:

 * the GNU Lesser General Public License as published by the Free
    Software Foundation; either version 3 of the License, or (at your
    option) any later version.

   or

 * the GNU General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option) any
    later version.

   or both in parallel, as here.

   The GNU MP Library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received copies of the GNU General Public License and the
   GNU Lesser General Public License along with the GNU MP Library.  If not,
   see https://www.gnu.org/licenses/.  */

#include "gmp-impl.h"
#include <stdio.h>


#ifdef OPERATION_add
#define FUNCTION     mpz_add
#define VARIATION
#endif
#ifdef OPERATION_sub
#define FUNCTION     mpz_sub
#define VARIATION    -
#endif

#ifndef FUNCTION
Error, need OPERATION_add or OPERATION_sub
#endif

// sum = a + b
void
FUNCTION (mpz_ptr sum, mpz_srcptr a, mpz_srcptr b)
{

  //printf("(?) => mpz_add\n");

  mp_srcptr a_ptr, b_ptr;
  mp_ptr sum_ptr;
  mp_size_t a_size, b_size, sum_size;
  mp_size_t a_size_abs;
  mp_size_t b_size_abs;

  a_size = SIZ(a);
  b_size = VARIATION SIZ(b);
  a_size_abs = ABS (a_size);
  b_size_abs = ABS (b_size);

  if (a_size_abs < b_size_abs)
  {
    /* Swap a and b. */
    MPZ_SRCPTR_SWAP (a, b);
    MP_SIZE_T_SWAP (a_size, b_size);
    MP_SIZE_T_SWAP (a_size_abs, b_size_abs);
  }

  /* True: a_size_abs >= b_size_abs.  */
  /* a is larger than b! */

  /* If not space for sum (and possible carry), increase space. */
  sum_size = a_size_abs + 1;
  sum_ptr = MPZ_REALLOC (sum, sum_size);

  /* These must be after realloc (a or b may be the same as sum). */
  a_ptr = PTR(a);
  b_ptr = PTR(b);

  if ((a_size ^ b_size) < 0)
  {
    /* a and b have different sign.  Need to compare them to determine
       which operand to subtract from which.  */

    /* This test is right since a_size_abs >= b_size_abs. */
    if (a_size_abs != b_size_abs)
    {
      mpn_sub (sum_ptr, a_ptr, a_size_abs, b_ptr, b_size_abs);
      sum_size = a_size_abs;
      MPN_NORMALIZE (sum_ptr, sum_size);
      if (a_size < 0)
        sum_size = -sum_size;
    }
    else if (mpn_cmp (a_ptr, b_ptr, a_size_abs) < 0)
    {
      mpn_sub_n (sum_ptr, b_ptr, a_ptr, a_size_abs);
      sum_size = a_size_abs;
      MPN_NORMALIZE (sum_ptr, sum_size);
      if (a_size >= 0)
        sum_size = -sum_size;
    }
    else
    {
      mpn_sub_n (sum_ptr, a_ptr, b_ptr, a_size_abs);
      sum_size = a_size_abs;
      MPN_NORMALIZE (sum_ptr, sum_size);
      if (a_size < 0)
        sum_size = -sum_size;
    }
  }
  else
  {
    /* a and b have same sign. Add them. */
    // Actually a_size_abs doesn't matter
    mp_limb_t cy_limb = mpn_add (sum_ptr, a_ptr, a_size_abs, b_ptr, b_size_abs);
    sum_ptr[a_size_abs] = cy_limb;
    sum_size = a_size_abs + cy_limb;

    if (a_size < 0)
      sum_size = -sum_size;
  }

  SIZ(sum) = sum_size;
}
